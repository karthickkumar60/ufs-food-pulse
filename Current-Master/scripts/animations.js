$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        loop: false,
        items: 1,
        center: true,
        nav: true,
        autoplay: true,
        autoplayHoverPause: true,
        smartSpeed: 500,
        rewind: true,
        navText: ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="-471 251.6 16.8 59.4" class="svg-slider"><polygon class="st0" points="-470,281.3 -454,251.6 -453.2,253 -468.4,281.3 -453.2,309.6 -454,311 	"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" viewBox="-246.5 367.6 16.8 59.4" class="svg-slider"><polygon class="st0" points="-245.7,427 -246.5,425.6 -231.3,397.3 -246.5,369 -245.7,367.6 -229.7,397.3 	"/></svg>']
    });
  });
  
document.addEventListener("DOMContentLoaded", function() {

    // setTimeout(function() {
    //     /* ADDTHIS BAR */
    //     var barTop = document.getElementById('at-share-dock'),
    //         body = document.getElementsByTagName('body')[0];

    //     if (barTop) {
    //         if (!barTop.classList.contains('at4-hide') && !body.classList.contains('body--mobile')) {
    //             body.classList.add('body--mobile');
    //         } else if (barTop.classList.contains('at4-hide') && body.classList.contains('body--mobile')) {
    //             body.classList.remove('body--mobile');
    //         }
    //     }
    // }, 1000);

    /* BACK TO TOP */
    var backToTopButton = document.getElementById('back-to-top');

    backToTopButton.addEventListener("click", function() {
        scrollToTop(1200);
    });

    /* ARROWS */
    // setInterval(function() {
    //     arrowActiveChange();
    // }, 1000);

    /* SLIDER */
    // var step = 0,
    //     clicked = false,
    //     arrowRight = document.getElementsByClassName('first-section__arrow--right')[0],
    //     arrowLeft = document.getElementsByClassName('first-section__arrow--left')[0],
    //     items = document.getElementsByClassName('first-section__item'),
    //     buttons = document.getElementsByClassName('first-section__slider-button'),
    //     interval;

    // startInterval();

    // arrowRight.addEventListener("click", function() {
    //     slideToRight();
    //     clicked = true;
    // });

    // function slideToRight() {
    //     buttons[step].classList.replace('first-section__slider-button--active');
    //     items[step].classList.remove('first-section__item--active');
    //     step = ((step < 2) ? step += 1 : 0);
    //     buttons[step].classList.add('first-section__slider-button--active');
    //     items[step].classList.add('first-section__item--active');
    // }
    
    // function startInterval() {
    //     interval = setInterval(function() {
    //         if (clicked == false) {
    //             slideToRight();
    //         } else {
    //             clicked = false;
    //         }            
    //     }, 5000);
    // }

    /* VIDEO */
    var playButton = document.getElementsByClassName('first-section-video__play-button')[0],
        closeButton = document.getElementsByClassName('video-lightbox__close-button')[0];
    playButton.addEventListener('click', playVideo);
    closeButton.addEventListener('click', closeVideo);

});

function scrollToTop(scrollDuration) {
    var scrollStep = -window.scrollY / (scrollDuration / 15),
        scrollInterval = setInterval(function() {
            if (window.scrollY != 0) {
                window.scrollBy(0, scrollStep);
            } else {
                clearInterval(scrollInterval);
            }
        }, 15);
}

function playVideo() {
    var lightbox = document.getElementsByClassName('video-lightbox')[0],
        video = document.getElementById('presentation-video'),
        addthisBar = document.getElementsByClassName('addthis-smartlayers-mobile')[0];
    lightbox.classList.add('video-lightbox--visible');
    if (window.innerWidth <= 769) {
        addthisBar.classList.add('hidden');
        // if (video.requestFullscreen) {
        //     video.requestFullscreen();
        // }
        // else if (video.msRequestFullscreen) {
        //     video.msRequestFullscreen();
        // }
        // else if (video.mozRequestFullScreen) {
        //     video.mozRequestFullScreen();
        // }
        // else if (video.webkitRequestFullScreen) {
        //     video.webkitRequestFullScreen();
        // }
    }
    
    video.play();
}

function closeVideo() {
    var lightbox = document.getElementsByClassName('video-lightbox')[0],
        video = document.getElementById('presentation-video'),
        addthisBar = document.getElementsByClassName('addthis-smartlayers-mobile')[0];

    if (video.currentTime > 0 && video.paused == false && video.ended == false && video.readyState < video.HAVE_FUTURE_DATA) {
        video.pause();
    }

    if (lightbox.classList.contains('video-lightbox--visible')) {
        lightbox.classList.remove('video-lightbox--visible');
    }

    if (addthisBar.classList.contains('hidden')) {
        addthisBar.classList.remove('hidden');
    }
}

// function arrowActiveChange() {
//     var arrows = document.getElementsByClassName("svg-arrow");

//     if (arrows[0].classList.contains("svg-arrow--active")) {
//         arrows[0].classList.remove("svg-arrow--active");
//         arrows[1].classList.add("svg-arrow--active");
//     } else if (arrows[1].classList.contains("svg-arrow--active")) {
//         arrows[1].classList.remove("svg-arrow--active");
//         arrows[0].classList.add("svg-arrow--active");
//     } else {
//         arrows[1].classList.add("svg-arrow--active");
//     }
// }